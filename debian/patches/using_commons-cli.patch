Description: using classes of commons-cli instead of unpackaged ones
Author: Pierre Gruet <pgt@debian.org>
Forwarded: not-needed
Last-Update: 2021-07-31

--- a/goby-distribution/src/main/java/org/campagnelab/goby/algorithmic/algorithm/dmr/EstimatedDistribution.java
+++ b/goby-distribution/src/main/java/org/campagnelab/goby/algorithmic/algorithm/dmr/EstimatedDistribution.java
@@ -19,7 +19,7 @@
 package org.campagnelab.goby.algorithmic.algorithm.dmr;
 
 import org.campagnelab.goby.algorithmic.algorithm.FenwickTree;
-import edu.mssm.crover.cli.CLI;
+import org.apache.commons.cli.*;
 import it.unimi.dsi.fastutil.ints.IntArrayList;
 import it.unimi.dsi.fastutil.io.BinIO;
 import it.unimi.dsi.fastutil.objects.ObjectArrayList;
@@ -240,68 +240,113 @@
     }
 
     public static void main(final String[] args) throws IOException {
-        boolean printDensity = CLI.isKeywordGiven(args, "--print-density");
-        boolean printObservations = CLI.isKeywordGiven(args, "--print-observations");
-        String filename = CLI.getOption(args, "-f", null);
-        String outputFilename = CLI.getOption(args, "-o", "out.tsv");
-        PrintWriter outWriter = new PrintWriter(new FileWriter(outputFilename));
-        if (printDensity) {
-            EstimatedDistribution estimated = null;
-
-            try {
-                estimated = load(filename);
-                String statName = estimated.getStatAdaptor().statName();
-                System.out.println("Statistic=" + statName);
-                System.out.println("binning strategy=" + estimated.getBinningStrategy().getName());
-                int index = 0;
-                outWriter.println("midPointSumTotal\tsumTotal range\t" + statName + "\tcount-at-" + statName);
-                final BinningStrategy binningStrategy = estimated.getBinningStrategy();
-                for (final FenwickTree tree : estimated.densitities) {
-                    if (tree != null) {
-                        int low = binningStrategy.getLowerBound(index);
-                        int high = binningStrategy.getUpperBound(index);
-
-                        int midPointSumTotal = binningStrategy.getMidpoint(index);
-                        System.out.printf("low=%d high=%d midPoint=%d %n", low, high, midPointSumTotal);
-                        final long maxCumulative = tree.getCumulativeCount(tree.size() - 2);
-                        for (int scaledStatistic = 0; scaledStatistic < tree.size() - 1; scaledStatistic++) {
-                            final long cumulativeCountAt = tree.getCumulativeCount(scaledStatistic);
-                            final long cumulativeCountAfter = tree.getCumulativeCount(scaledStatistic + 1);
-
-                            outWriter.printf("%d\t[%d-%d]\t%g\t%d%n", midPointSumTotal, binningStrategy.getLowerBound(index),
-                                    binningStrategy.getUpperBound(index), estimated.unscale(scaledStatistic), cumulativeCountAfter - cumulativeCountAt);
-                            if (cumulativeCountAfter == maxCumulative) {
-                                break;
+        Options options = new Options();
+
+        Option pDensity = new Option("printDensity", "print-density", false, "print density");
+        pDensity.setRequired(false);
+        options.addOption(pDensity);
+
+        Option pObservations = new Option("printObservations", "print-observations", false, "print observations");
+        pObservations.setRequired(false);
+        options.addOption(pObservations);
+
+        Option fname = new Option("filename", "filename", true, "filename");
+        fname.setRequired(false);
+        options.addOption(fname);
+
+        Option o = new Option("o", "o", true, "output filename");
+        o.setRequired(false);
+        options.addOption(o);
+
+        CommandLineParser parser = new DefaultParser();
+        CommandLine cmd;
+        String filename;
+        String outputFilename;
+        boolean printDensity;
+        boolean printObservations;
+
+        try {
+            cmd = parser.parse(options, args);
+            printDensity = cmd.hasOption("printDensity");
+
+            printObservations = cmd.hasOption("printObservations");
+
+            if (cmd.hasOption("f")) {
+                filename = cmd.getOptionValue("f");
+            }
+            else {
+                filename = null;
+            }
+
+            if (cmd.hasOption("o")) {
+                outputFilename = cmd.getOptionValue("o");
+            }
+            else {
+                outputFilename = new String("out.tsv");
+            }
+
+            PrintWriter outWriter = new PrintWriter(new FileWriter(outputFilename));
+            if (printDensity) {
+                EstimatedDistribution estimated = null;
+
+                try {
+                    estimated = load(filename);
+                    String statName = estimated.getStatAdaptor().statName();
+                    System.out.println("Statistic=" + statName);
+                    System.out.println("binning strategy=" + estimated.getBinningStrategy().getName());
+                    int index = 0;
+                    outWriter.println("midPointSumTotal\tsumTotal range\t" + statName + "\tcount-at-" + statName);
+                    final BinningStrategy binningStrategy = estimated.getBinningStrategy();
+                    for (final FenwickTree tree : estimated.densitities) {
+                        if (tree != null) {
+                            int low = binningStrategy.getLowerBound(index);
+                            int high = binningStrategy.getUpperBound(index);
+
+                            int midPointSumTotal = binningStrategy.getMidpoint(index);
+                            System.out.printf("low=%d high=%d midPoint=%d %n", low, high, midPointSumTotal);
+                            final long maxCumulative = tree.getCumulativeCount(tree.size() - 2);
+                            for (int scaledStatistic = 0; scaledStatistic < tree.size() - 1; scaledStatistic++) {
+                                final long cumulativeCountAt = tree.getCumulativeCount(scaledStatistic);
+                                final long cumulativeCountAfter = tree.getCumulativeCount(scaledStatistic + 1);
+
+                                outWriter.printf("%d\t[%d-%d]\t%g\t%d%n", midPointSumTotal, binningStrategy.getLowerBound(index),
+                                        binningStrategy.getUpperBound(index), estimated.unscale(scaledStatistic), cumulativeCountAfter - cumulativeCountAt);
+                                if (cumulativeCountAfter == maxCumulative) {
+                                    break;
+                                }
                             }
                         }
-                    }
-                    index++;
+                        index++;
 
+                    }
+                    outWriter.close();
+                } catch (Exception e) {
+                    e.printStackTrace();
+                    System.exit(1);
                 }
-                outWriter.close();
-            } catch (Exception e) {
-                e.printStackTrace();
-                System.exit(1);
-            }
 
-        }
-        if (printObservations) {
+            }
+            if (printObservations) {
 
-            EstimatedDistribution estimated = null;
+                EstimatedDistribution estimated = null;
 
-            try {
-                estimated = load(filename);
-                String statName = estimated.getStatAdaptor().statName();
-                outWriter.println("tscaled-" + statName + "\t" + statName + "\tcovariates\n");
-                for (final Observation observation : estimated.getObservations()) {
-                    outWriter.printf("%d\t%d\t%g\t%s%n", observation.scaledStatistic,
-                            estimated.unscale(observation.scaledStatistic), IntArrayList.wrap(observation.covariates));
+                try {
+                    estimated = load(filename);
+                    String statName = estimated.getStatAdaptor().statName();
+                    outWriter.println("tscaled-" + statName + "\t" + statName + "\tcovariates\n");
+                    for (final Observation observation : estimated.getObservations()) {
+                        outWriter.printf("%d\t%d\t%g\t%s%n", observation.scaledStatistic,
+                                estimated.unscale(observation.scaledStatistic), IntArrayList.wrap(observation.covariates));
+                    }
+                    outWriter.close();
+                } catch (ClassNotFoundException e) {
+                    e.printStackTrace();
+                    System.exit(1);
                 }
-                outWriter.close();
-            } catch (ClassNotFoundException e) {
-                e.printStackTrace();
-                System.exit(1);
             }
+        } catch (ParseException e) {
+            System.out.println(e.getMessage());
+            System.exit(1);
         }
     }
 
--- a/goby-distribution/src/main/java/org/campagnelab/goby/cli/FoldChangeForExonPairs.java
+++ b/goby-distribution/src/main/java/org/campagnelab/goby/cli/FoldChangeForExonPairs.java
@@ -19,7 +19,7 @@
 package org.campagnelab.goby.cli;
 
 import edu.cornell.med.icb.io.TSVReader;
-import edu.mssm.crover.cli.CLI;
+import org.apache.commons.cli.*;
 
 import java.io.FileReader;
 import java.io.FileNotFoundException;
@@ -46,38 +46,109 @@
     }
 
     private void process(String[] args) throws FileNotFoundException {
-        String inputFilenames = CLI.getOption(args, "--data", "expression-data.tsv,expression-data2.tsv");
-        String pairsFilename = CLI.getOption(args, "--pairs", "pairs.tsv");
-        String log2AverageId = CLI.getOption(args, "--group", "average log2_RPKM group UHR(BUQ)");
-        String outputFilename = CLI.getOption(args, "--output", "/data/gc-weights/exons/out.tsv");
-        String averageCountId = CLI.getOption(args, "--threshold-id", "average count group Brain");
-        int thresholdValue = CLI.getIntOption(args, "--threshold-value", 20);
-
-        //  String groupB= CLI.getOption(args,"-2","average RPKM group UHR(BUQ)");
-        int log2AverageColumnIndex = -1;
-        Object2DoubleMap<MutableString> exonExpressionData;
-
-
-        ObjectArrayList<Pair> pairs = loadPairs(pairsFilename);
-        PrintWriter output = new PrintWriter(outputFilename);
-        output.printf("experiment\texonId1\texonId2\tlog2RpkmExon1-log2RpkmExon2%n");
-        for (String inputFilename : inputFilenames.split("[,]")) {
-
-            System.out.println("Processing " + inputFilename);
-            exonExpressionData = loadData(inputFilename, log2AverageId, averageCountId, thresholdValue);
-            System.out.println("Size: " + exonExpressionData.size());
-            System.out.println("Writing data..");
-            for (Pair pair : pairs) {
-                //calculate log2 RPKM exonId1 - exonId2:
-                if (exonExpressionData.containsKey(pair.exonId1) && exonExpressionData.containsKey(pair.exonId2)) {
-                    double log2FoldChangeExons = exonExpressionData.get(pair.exonId1) - exonExpressionData.get(pair.exonId2);
-                    output.printf("%s\t%s\t%s\t%g%n", FilenameUtils.getBaseName(inputFilename),
-                            pair.exonId1, pair.exonId2, log2FoldChangeExons);
-                }
+        Options options = new Options();
+
+        Option oData = new Option("data", "data", true, "data");
+        oData.setRequired(false);
+        options.addOption(oData);
+
+        Option oPairs = new Option("pairs", "pairs", true, "pairs");
+        oPairs.setRequired(false);
+        options.addOption(oPairs);
+
+        Option oGroup = new Option("group", "group", true, "group");
+        oGroup.setRequired(false);
+        options.addOption(oGroup);
+
+        Option oOutput = new Option("output", "output", true, "output");
+        oOutput.setRequired(false);
+        options.addOption(oOutput);
+
+        Option oThresId = new Option("thresholdId", "threshold-id", true, "threshold id");
+        oThresId.setRequired(false);
+        options.addOption(oThresId);
+
+        Option oThresVal = new Option("thresholdVal", "threshold-value", true, "threshold value");
+        oThresVal.setRequired(false);
+        options.addOption(oThresVal);
+
+        CommandLineParser parser = new DefaultParser();
+        CommandLine cmd;
+        String inputFilenames;
+        String pairsFilename;
+        String log2AverageId;
+        String outputFilename;
+        String averageCountId;
+        int thresholdValue;
+
+        try {
+            cmd = parser.parse(options, args);
 
+            if (cmd.hasOption("data")) {
+                inputFilenames = cmd.getOptionValue("data");
+            }
+            else {
+                inputFilenames = new String("expression-data.tsv,expression-data2.tsv");
+            }
+            if (cmd.hasOption("pairs")) {
+                pairsFilename = cmd.getOptionValue("pairs");
+            }
+            else {
+                pairsFilename = new String("pairs.tsv");
+            }
+            if (cmd.hasOption("group")) {
+                log2AverageId = cmd.getOptionValue("group");
+            }
+            else {
+                log2AverageId = new String("average log2_RPKM group UHR(BUQ)");
+            }
+            if (cmd.hasOption("output")) {
+                outputFilename = cmd.getOptionValue("output");
+            }
+            else {
+                outputFilename = new String("/data/gc-weights/exons/out.tsv");
+            }
+            if (cmd.hasOption("thresholdId")) {
+                averageCountId = cmd.getOptionValue("thresholdId");
+            }
+            else {
+                averageCountId = new String("average count group Brain");
+            }
+            if (cmd.hasOption("thresholdVal")) {
+                thresholdValue = Integer.parseInt(cmd.getOptionValue("thresholdVal"));
+            }
+            else {
+                thresholdValue = 20;
             }
-        }
 
+            //  String groupB= CLI.getOption(args,"-2","average RPKM group UHR(BUQ)");
+            int log2AverageColumnIndex = -1;
+            Object2DoubleMap<MutableString> exonExpressionData;
+
+
+            ObjectArrayList<Pair> pairs = loadPairs(pairsFilename);
+            PrintWriter output = new PrintWriter(outputFilename);
+            output.printf("experiment\texonId1\texonId2\tlog2RpkmExon1-log2RpkmExon2%n");
+            for (String inputFilename : inputFilenames.split("[,]")) {
+
+                System.out.println("Processing " + inputFilename);
+                exonExpressionData = loadData(inputFilename, log2AverageId, averageCountId, thresholdValue);
+                System.out.println("Size: " + exonExpressionData.size());
+                System.out.println("Writing data..");
+                for (Pair pair : pairs) {
+                    //calculate log2 RPKM exonId1 - exonId2:
+                    if (exonExpressionData.containsKey(pair.exonId1) && exonExpressionData.containsKey(pair.exonId2)) {
+                        double log2FoldChangeExons = exonExpressionData.get(pair.exonId1) - exonExpressionData.get(pair.exonId2);
+                        output.printf("%s\t%s\t%s\t%g%n", FilenameUtils.getBaseName(inputFilename),
+                                pair.exonId1, pair.exonId2, log2FoldChangeExons);
+                    }
+
+                }
+            }
+        } catch (ParseException e) {
+            System.out.println(e.getMessage());
+            System.exit(1);
+        }
     }
 
     private ObjectArrayList<Pair> loadPairs(String pairsFilename) throws FileNotFoundException {
--- a/goby-distribution/src/main/java/org/campagnelab/goby/cli/PlantIndels.java
+++ b/goby-distribution/src/main/java/org/campagnelab/goby/cli/PlantIndels.java
@@ -19,7 +19,7 @@
 package org.campagnelab.goby.cli;
 
 import org.campagnelab.goby.parsers.FastaParser;
-import edu.mssm.crover.cli.CLI;
+import org.apache.commons.cli.*;
 import it.unimi.dsi.lang.MutableString;
 
 import java.io.*;
@@ -40,26 +40,128 @@
     private int numIndels;
 
     public static void main(final String args[]) throws IOException {
-        final String fastaReference = CLI.getOption(args, "-r", null);
-        final String outputFilename = CLI.getOption(args, "-o", "out.fq");
-        final String regionIndelTruth = CLI.getOption(args, "-t", "indel-truth.tsv");
-        // reference sequence to use
-        final String refChoice = CLI.getOption(args, "-c", "22");
-        int from = CLI.getIntOption(args, "-s", 0);
-        int to = CLI.getIntOption(args, "-e", 0);
 
-        if (to < from) {
-            System.err.println("argument to -e must be larger than argument to -s");
-            System.exit(1);
-        }
-        final int indelLength = CLI.getIntOption(args, "-l", 3);
+        Options options = new Options();
+
+        Option oR = new Option("r", "r", true, "r");
+        oR.setRequired(false);
+        options.addOption(oR);
+
+        Option oO = new Option("o", "o", true, "o");
+        oO.setRequired(false);
+        options.addOption(oO);
+
+        Option oT = new Option("t", "t", true, "t");
+        oT.setRequired(false);
+        options.addOption(oT);
+
+        Option oC = new Option("c", "c", true, "c");
+        oC.setRequired(false);
+        options.addOption(oC);
+
+        Option oS = new Option("s", "s", true, "s");
+        oS.setRequired(false);
+        options.addOption(oS);
+
+        Option oE = new Option("e", "e", true, "e");
+        oE.setRequired(false);
+        options.addOption(oE);
+
+        Option oL = new Option("l", "l", true, "l");
+        oL.setRequired(false);
+        options.addOption(oL);
+
+        Option oM = new Option("m", "m", true, "m");
+        oM.setRequired(false);
+        options.addOption(oM);
+
+        Option oRd = new Option("readDeletion", "read-deletion", false, "readDeletion");
+        oRd.setRequired(false);
+        options.addOption(oRd);
+
+        Option oN = new Option("n", "n", true, "n");
+        oN.setRequired(false);
+        options.addOption(oN);
+
+        CommandLineParser parser = new DefaultParser();
+        CommandLine cmd;
+        final String fastaReference;
+        final String outputFilename;
+        final String regionIndelTruth;
+        // reference sequence to use
+        final String refChoice;
+        int from;
+        int to;
+        final int indelLength;
         PlantIndels processor = new PlantIndels();
-        processor.outputFilename = outputFilename;
-        processor.insert = !CLI.isKeywordGiven(args, "--read-deletion");
-        processor.numIndels=CLI.getIntOption(args,"-n",5);
-        processor.process(refChoice, fastaReference, from, to, indelLength, regionIndelTruth);
 
+        try {
+            cmd = parser.parse(options, args);
 
+            if (cmd.hasOption("r")) {
+                fastaReference = cmd.getOptionValue("r");
+            }
+            else {
+                fastaReference = null;
+            }
+            if (cmd.hasOption("o")) {
+                outputFilename = cmd.getOptionValue("o");
+            }
+            else {
+                outputFilename = new String("out.fq");
+            }
+            if (cmd.hasOption("t")) {
+                regionIndelTruth = cmd.getOptionValue("t");
+            }
+            else {
+                regionIndelTruth = new String("indel-truth.tsv");
+            }
+            if (cmd.hasOption("c")) {
+                refChoice = cmd.getOptionValue("c");
+            }
+            else {
+                refChoice = new String("22");
+            }
+            if (cmd.hasOption("s")) {
+                from = Integer.parseInt(cmd.getOptionValue("s"));
+            }
+            else {
+                from = 0;
+            }
+            if (cmd.hasOption("e")) {
+                to = Integer.parseInt(cmd.getOptionValue("e"));
+            }
+            else {
+                to = 0;
+            }
+
+            if (to < from) {
+                System.err.println("argument to -e must be larger than argument to -s");
+                System.exit(1);
+            }
+
+            if (cmd.hasOption("l")) {
+                indelLength = Integer.parseInt(cmd.getOptionValue("l"));
+            }
+            else {
+                indelLength = 3;
+            }
+
+            processor.outputFilename = outputFilename;
+            processor.insert = !cmd.hasOption("readDeletion");
+
+            if (cmd.hasOption("n")) {
+                processor.numIndels = Integer.parseInt(cmd.getOptionValue("n"));
+            }
+            else {
+                processor.numIndels = 5;
+            }
+
+            processor.process(refChoice, fastaReference, from, to, indelLength, regionIndelTruth);
+        } catch (ParseException e) {
+            System.out.println(e.getMessage());
+            System.exit(1);
+        }
     }
 
     public void process(String refChoice, String fastaReference, int from, int to, int indelLength, String regionIndelTruth) throws IOException {
--- a/goby-distribution/src/main/java/org/campagnelab/goby/cli/SimulateBisulfiteReads.java
+++ b/goby-distribution/src/main/java/org/campagnelab/goby/cli/SimulateBisulfiteReads.java
@@ -20,7 +20,7 @@
 
 import org.campagnelab.goby.reads.QualityEncoding;
 import org.campagnelab.goby.parsers.FastaParser;
-import edu.mssm.crover.cli.CLI;
+import org.apache.commons.cli.*;
 import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
 import it.unimi.dsi.fastutil.doubles.DoubleIterator;
 import it.unimi.dsi.fastutil.doubles.DoubleList;
@@ -72,32 +72,152 @@
 
     public static void main(String[] args) throws IOException {
 
-        String fastaReference = CLI.getOption(args, "-r", null);
-        String outputFilename = CLI.getOption(args, "-o", "out.fq");
-        String regionTrueRates = CLI.getOption(args, "-t", "true-methylation.tsv");
+        Options options = new Options();
+
+        Option oR = new Option("r", "r", true, "r");
+        oR.setRequired(false);
+        options.addOption(oR);
+
+        Option oO = new Option("o", "o", true, "o");
+        oO.setRequired(false);
+        options.addOption(oO);
+
+        Option oT = new Option("t", "t", true, "t");
+        oT.setRequired(false);
+        options.addOption(oT);
+
+        Option oC = new Option("c", "c", true, "c");
+        oC.setRequired(false);
+        options.addOption(oC);
+
+        Option oS = new Option("s", "s", true, "s");
+        oS.setRequired(false);
+        options.addOption(oS);
+
+        Option oE = new Option("e", "e", true, "e");
+        oE.setRequired(false);
+        options.addOption(oE);
+
+        Option oL = new Option("l", "l", true, "l");
+        oL.setRequired(false);
+        options.addOption(oL);
+
+        Option oM = new Option("m", "m", true, "m");
+        oM.setRequired(false);
+        options.addOption(oM);
+
+        Option oBs = new Option("bisulfite", "bisulfite", false, "bisulfite");
+        oBs.setRequired(false);
+        options.addOption(oBs);
+
+        Option oStrand = new Option("strand", "strand", true, "strand");
+        oStrand.setRequired(false);
+        options.addOption(oStrand);
+
+        Option oN = new Option("n", "n", true, "n");
+        oN.setRequired(false);
+        options.addOption(oN);
+
+        CommandLineParser parser = new DefaultParser();
+        CommandLine cmd;
+        String fastaReference;
+        String outputFilename;
+        String regionTrueRates;
         // reference sequence to use
-        String refChoice = CLI.getOption(args, "-c", "22");
-        int from = CLI.getIntOption(args, "-s", 0);
-        int to = CLI.getIntOption(args, "-e", 0);
-        if (to < from) {
-            System.err.println("argument to -e must be larger than argument to -s");
-            System.exit(1);
-        }
-        int readLength = CLI.getIntOption(args, "-l", 50);
-        String methylationRateFilename = CLI.getOption(args, "-m", "methylation-rates.tsv");
+        String refChoice;
+        int from;
+        int to;
+        int readLength;
+        String methylationRateFilename;
+        String strandChoice;
         SimulateBisulfiteReads processor = new SimulateBisulfiteReads();
-        final boolean bisulfite = CLI.isKeywordGiven(args, "--bisulfite");
-        String strandChoice = CLI.getOption(args, "--strand", "both");
+        final boolean bisulfite;
 
-        processor.configure(bisulfite, strandChoice);
-        processor.bisulfiteTreatment = bisulfite;
-        processor.readLength = readLength;
-        processor.outputFilename = outputFilename;
-        processor.regionTrueRates = regionTrueRates;
-        processor.numRepeats = CLI.getIntOption(args, "-n", 10000);
-
-        processor.process(refChoice, fastaReference, from, to, methylationRateFilename);
+        try {
+            cmd = parser.parse(options, args);
 
+            if (cmd.hasOption("r")) {
+                fastaReference = cmd.getOptionValue("r");
+            }
+            else {
+                fastaReference = null;
+            }
+            if (cmd.hasOption("o")) {
+                outputFilename = cmd.getOptionValue("o");
+            }
+            else {
+                outputFilename = new String("out.fq");
+            }
+            if (cmd.hasOption("t")) {
+                regionTrueRates = cmd.getOptionValue("t");
+            }
+            else {
+                regionTrueRates = new String("true-methylation.tsv");
+            }
+            if (cmd.hasOption("c")) {
+                refChoice = cmd.getOptionValue("c");
+            }
+            else {
+                refChoice = new String("22");
+            }
+            if (cmd.hasOption("s")) {
+                from = Integer.parseInt(cmd.getOptionValue("s"));
+            }
+            else {
+                from = 0;
+            }
+            if (cmd.hasOption("e")) {
+                to = Integer.parseInt(cmd.getOptionValue("e"));
+            }
+            else {
+                to = 0;
+            }
+
+            if (to < from) {
+                System.err.println("argument to -e must be larger than argument to -s");
+                System.exit(1);
+            }
+
+            if (cmd.hasOption("l")) {
+                readLength = Integer.parseInt(cmd.getOptionValue("l"));
+            }
+            else {
+                readLength = 50;
+            }
+            if (cmd.hasOption("m")) {
+                methylationRateFilename = cmd.getOptionValue("c");
+            }
+            else {
+                methylationRateFilename = new String("methylation-rates.tsv");
+            }
+
+            bisulfite = cmd.hasOption("biSulfite");
+
+            if (cmd.hasOption("strand")) {
+                strandChoice = cmd.getOptionValue("strang");
+            }
+            else {
+                strandChoice = new String("both");
+            }
+
+            processor.configure(bisulfite, strandChoice);
+            processor.bisulfiteTreatment = bisulfite;
+            processor.readLength = readLength;
+            processor.outputFilename = outputFilename;
+            processor.regionTrueRates = regionTrueRates;
+
+            if (cmd.hasOption("n")) {
+                processor.numRepeats = Integer.parseInt(cmd.getOptionValue("n"));
+            }
+            else {
+                processor.numRepeats = 10000;
+            }
+
+            processor.process(refChoice, fastaReference, from, to, methylationRateFilename);
+        } catch (ParseException e) {
+            System.out.println(e.getMessage());
+            System.exit(1);
+        }
     }
 
     public void configure(boolean bisulfite, String strandChoice) {
--- a/goby-distribution/src/main/java/org/campagnelab/goby/methylation/MethylSimilarityScan.java
+++ b/goby-distribution/src/main/java/org/campagnelab/goby/methylation/MethylSimilarityScan.java
@@ -20,7 +20,7 @@
 
 import org.campagnelab.goby.cli.DoInParallel;
 import edu.cornell.med.icb.io.TSVReader;
-import edu.mssm.crover.cli.CLI;
+import org.apache.commons.cli.*;
 import it.unimi.dsi.fastutil.floats.FloatArrayList;
 import it.unimi.dsi.fastutil.ints.*;
 import it.unimi.dsi.fastutil.io.BinIO;
@@ -63,40 +63,92 @@
     }
 
     private void process(String[] args) throws IOException {
-        String inputFilename = CLI.getOption(args, "-i", "/data/lister/mc_h1.tsv");
-        this.windowWidths = CLI.getOption(args, "-w", "10");
-        this.maxBestHits = CLI.getIntOption(args, "-h", 100);
-        String outputFilename = CLI.getOption(args, "-o", "out.tsv");
-        final MethylationData data = load(inputFilename);
-        File outputFile = new File(outputFilename);
-        boolean outputFileExists = outputFile.exists();
-// append:
-        PrintWriter output = new PrintWriter(new FileWriter(outputFilename, true));
 
+        Options options = new Options();
 
-        if (!outputFileExists) {
-            output.write("windowSize\tlocation\tchromosome\tforward strand start\tforward strand end\treverse strand start\treverse strand end\teffective window size\tstatistic\n");
-        }
-        for (String windowWidthString : windowWidths.split("[,]")) {
-            final int windowWidth = Integer.parseInt(windowWidthString);
-            System.out.println("Processing window size=" + windowWidth);
-            final HitBoundedPriorityQueue hits = new HitBoundedPriorityQueue(maxBestHits);
-            DoInParallel scan = new DoInParallel() {
-                @Override
-                public void action(DoInParallel forDataAccess, String chromosome, int loopIndex) {
-                    compareStrands(hits, data, windowWidth, new MutableString(chromosome));
-                }
-            };
+        Option oI = new Option("i", "i", true, "i");
+        oI.setRequired(false);
+        options.addOption(oI);
+
+        Option oW = new Option("w", "w", true, "w");
+        oW.setRequired(false);
+        options.addOption(oW);
+
+        Option oH = new Option("h", "h", true, "h");
+        oH.setRequired(false);
+        options.addOption(oH);
+
+        Option oO = new Option("o", "o", true, "o");
+        oO.setRequired(false);
+        options.addOption(oO);
+
+        CommandLineParser parser = new DefaultParser();
+        CommandLine cmd;
+        String inputFilename;
+        String outputFilename;
+
+        try {
+            cmd = parser.parse(options, args);
+
+            if (cmd.hasOption("i")) {
+                inputFilename = cmd.getOptionValue("i");
+            }
+            else {
+                inputFilename = new String("/data/lister/mc_h1.tsv");
+            }
+            if (cmd.hasOption("w")) {
+                this.windowWidths = cmd.getOptionValue("w");
+            }
+            else {
+                this.windowWidths = new String("10");
+            }
+            if (cmd.hasOption("h")) {
+                this.maxBestHits = Integer.parseInt(cmd.getOptionValue("h"));
+            }
+            else {
+                this.maxBestHits = 100;
+            }
+            if (cmd.hasOption("o")) {
+                outputFilename = cmd.getOptionValue("o");
+            }
+            else {
+                outputFilename = new String("out.tsv");
+            }
 
-            try {
-                scan.execute(true, data.getChromosomeStrings());
-            } catch (Exception e) {
-                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
+            final MethylationData data = load(inputFilename);
+            File outputFile = new File(outputFilename);
+            boolean outputFileExists = outputFile.exists();
+            // append:
+            PrintWriter output = new PrintWriter(new FileWriter(outputFilename, true));
+
+
+            if (!outputFileExists) {
+                output.write("windowSize\tlocation\tchromosome\tforward strand start\tforward strand end\treverse strand start\treverse strand end\teffective window size\tstatistic\n");
             }
+            for (String windowWidthString : windowWidths.split("[,]")) {
+                final int windowWidth = Integer.parseInt(windowWidthString);
+                System.out.println("Processing window size=" + windowWidth);
+                final HitBoundedPriorityQueue hits = new HitBoundedPriorityQueue(maxBestHits);
+                DoInParallel scan = new DoInParallel() {
+                    @Override
+                    public void action(DoInParallel forDataAccess, String chromosome, int loopIndex) {
+                        compareStrands(hits, data, windowWidth, new MutableString(chromosome));
+                    }
+                };
+
+                try {
+                    scan.execute(true, data.getChromosomeStrings());
+                } catch (Exception e) {
+                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
+                }
 
-            printResults(hits, windowWidth, data, output);
+                printResults(hits, windowWidth, data, output);
+            }
+            output.close();
+        } catch (ParseException e) {
+            System.out.println(e.getMessage());
+            System.exit(1);
         }
-        output.close();
     }
 
 
--- a/goby-distribution/src/main/java/org/campagnelab/goby/modes/PercentMismatchesQualityFilter.java
+++ b/goby-distribution/src/main/java/org/campagnelab/goby/modes/PercentMismatchesQualityFilter.java
@@ -20,7 +20,7 @@
 
 package org.campagnelab.goby.modes;
 
-import edu.mssm.crover.cli.CLI;
+import org.apache.commons.cli.*;
 import org.apache.commons.lang.StringUtils;
 import org.campagnelab.goby.alignments.Alignments;
 import org.campagnelab.goby.alignments.filters.AlignmentQualityFilter;
@@ -61,9 +61,30 @@
     }
 
     public void setParameters(final String parameters) {
-        final String[] args = StringUtils.defaultString(parameters).split("[',=]");
-        qualityThresholdPercent = CLI.getDoubleOption(args, "threshold", 0.05d);
-        System.err.println("Setting quality threshold to " + qualityThresholdPercent);
+        final String parametersWithHyphens = StringUtils.replace(parameters, "threshold", "-threshold");
+        final String[] args = StringUtils.defaultString(parametersWithHyphens).split("[',=]");
+        Options options = new Options();
+
+        Option threshold = new Option("threshold", "threshold", true, "threshold");
+        threshold.setRequired(false);
+        options.addOption(threshold);
+        CommandLineParser parser = new DefaultParser();
+        CommandLine cmd;
+
+        try {
+            cmd = parser.parse(options, args);
+
+            if (cmd.hasOption("threshold")) {
+                qualityThresholdPercent = Double.parseDouble(cmd.getOptionValue("threshold"));
+            }
+            else {
+                qualityThresholdPercent = 0.05d;
+            }
+            System.err.println("Setting quality threshold to " + qualityThresholdPercent);
+        } catch (ParseException e) {
+            System.out.println(e.getMessage());
+            System.exit(1);
+        }
     }
 
     public void printUsage(final PrintStream out) {
