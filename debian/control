Source: libgoby-java
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Pierre Gruet <pgt@debian.org>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               maven-debian-helper,
               javahelper,
               default-jdk
Build-Depends-Indep: ant,
                     ant-contrib,
                     libjbzip2-java,
                     libcommons-collections3-java,
                     libcommons-configuration-java,
                     libcommons-exec-java,
                     libcommons-io-java,
                     libcommons-lang-java,
                     libcommons-logging-java,
                     libcommons-math-java,
                     libcommons-cli-java,
                     libdistlib-java,
                     liblog4j1.2-java,
                     libexec-maven-plugin-java,
                     libjaxb-api-java,
                     libmaven-assembly-plugin-java,
                     libmaven-antrun-plugin-java,
                     libbuild-helper-maven-plugin-java,
                     libmaven-dependency-plugin-java,
                     libmaven-source-plugin-java,
                     libmaven-javadoc-plugin-java,
                     libprotobuf-java,
                     libhtsjdk-java,
                     libfastutil-java,
                     protobuf-compiler,
                     libjsap-java,
                     libdsiutils-java,
                     libicb-utils-java,
                     libreflections-java,
                     libpj-java,
                     libprotobuf-dev,
                     pkg-config,
                     r-cran-rjava,
                     libeasymock-java <!nocheck>,
                     junit4 <!nocheck>,
                     testng <!nocheck>,
                     libpicard-java <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/libgoby-java/
Vcs-Git: https://salsa.debian.org/med-team/libgoby-java.git
Homepage: https://campagnelab.org/software/goby/
Rules-Requires-Root: no

Package: libgoby-io-java
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${maven:Depends},
         libcommons-collections3-java,
         libjbzip2-java,
         libpj-java
Recommends: ${java:Recommends},
            ${maven:OptionalDepends}
Description: IO API for goby
 Goby is a next-gen data management framework designed to facilitate the
 implementation of efficient data analysis pipelines.
 .
 Goby provides very efficient file formats to store next-generation sequencing
 data and intermediary analysis results.
 .
 This package contains the Goby IO API, including code necessary to read and
 write Goby file formats. It is released under the LGPL3 license.

Package: goby-java
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${maven:Depends},
         default-jre,
         libgoby-io-java (= ${binary:Version}),
         libdistlib-java,
         libjaxb-api-java,
         r-cran-rjava
Recommends: ${java:Recommends},
            ${maven:OptionalDepends}
Description: next-generation sequencing data and results analysis tool
 Goby is a next-gen data management framework designed to facilitate the
 implementation of efficient data analysis pipelines.
 .
 Goby provides very efficient file formats to store next-generation sequencing
 data and intermediary analysis results.
 .
 Goby also provides utilities that implement common next-gen data computations.
 These utilities are designed to be relatively easy to use, yet very efficient.
 .
 This package provides the entire Goby framework, including application
 programs (i.e., Goby modes). It is released under the GPL3 license.
